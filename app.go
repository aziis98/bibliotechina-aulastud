package bibliotechina

import (
	"html/template"
	"io"
	"log"
	"net/http"
	"net/url"
	"path/filepath"
	"time"

	"github.com/aziis98/bibliotechina/db"
	"github.com/aziis98/bibliotechina/utils"

	"github.com/gorilla/mux"
	"github.com/gorilla/sessions"
	"golang.org/x/crypto/bcrypt"

	"github.com/davecgh/go-spew/spew"
)

type Page struct {
	filename string
	*template.Template
}

// loadTemplate creates a new template from a file (also previously loads common partial templates)
func loadTemplate(filename string) *template.Template {
	// "filepath.Base(filename)" is a reverse-engineering trick to create the template without having to call ExecuteTemplate() with the correct name later.
	tpl := template.New(filepath.Base(filename))

	tpl.Funcs(template.FuncMap{
		"spew": func(a ...interface{}) string {
			return spew.Sdump(a...)
		},
	})

	_, err := tpl.ParseFiles(filename)
	if err != nil {
		log.Fatal(err)
	}

	_, err = tpl.ParseGlob("./web/views/partials/*.html")
	if err != nil {
		log.Fatal(err)
	}

	return tpl
}

// GetTemplate gets from cache or loads a template with the given filename
func (app *App) GetTemplate(filename string) *Page {
	if page, ok := app.templateCache[filename]; ok {
		return page
	}

	page := &Page{
		filename,
		loadTemplate(filename),
	}

	app.templateCache[filename] = page

	return page
}

func (page *Page) Render(w io.Writer, data interface{}) {
	page.Template.Execute(w, data)
}

func (page *Page) CompileAndRender(w io.Writer, data interface{}) {
	page.Template = loadTemplate(page.filename)
	page.Template.Execute(w, data)
}

type App struct {
	config             *AppConfig
	CookieSessionStore *sessions.CookieStore

	Router   *mux.Router
	Database *db.Database
	Server   *http.Server

	templateCache map[string]*Page
}

func (app *App) RenderPage(w http.ResponseWriter, page *Page, data utils.Object) {
	if app.config.Development != nil {
		data["ExtraHtml"] = app.config.Development.ExtraHtml
		page.CompileAndRender(w, data)
	} else {
		page.Render(w, data)
	}
}

// RenderMessaggio is a utility for rendering a common page
func (app *App) RenderMessaggio(w http.ResponseWriter, r *http.Request, data utils.Object) {
	messaggioPage := app.GetTemplate("./web/views/messaggio.html")

	app.utenteMiddleware(
		func(rw http.ResponseWriter, r *http.Request, utente *db.Utente) {
			app.RenderPage(w, messaggioPage, utils.Object{"Utente": utente}.Apply(data))
		},
	)(w, r)
}

func (app *App) Login(w http.ResponseWriter, r *http.Request, email, password string) error {

	utente, err := app.Database.GetUtenteByEmail(email)
	if err != nil {
		return err
	}

	// TODO: Just for now to easily login into all accounts
	if password != "" {
		err = bcrypt.CompareHashAndPassword([]byte(utente.PasswordSaltHash), []byte(password))
		if err != nil {
			return err
		}
	}

	session, err := app.CookieSessionStore.Get(r, "utente")
	if err != nil {
		return err
	}

	session.Values["id"] = utente.Id

	err = session.Save(r, w)
	if err != nil {
		return err
	}

	return nil
}

func (app *App) Logout(w http.ResponseWriter, r *http.Request) error {
	session, err := app.CookieSessionStore.Get(r, "utente")
	if err != nil {
		return err
	}

	delete(session.Values, "id")

	err = session.Save(r, w)
	if err != nil {
		return err
	}

	return nil
}

// utenteMiddleware is a middleware that extracts the user session if present from the DB. The third argument of type *db.Utente may be nil!
func (app *App) utenteMiddleware(authFunc func(http.ResponseWriter, *http.Request, *db.Utente)) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		// Ottiene l'utente dalla sessione corrente
		utente, err := app.GetUtenteFromRequest(w, r)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		authFunc(w, r, utente)
	}
}

// authMiddleware ensures that the user is logged and send to "/login" otherwise. The third argument of type *db.Utente is never nil!
func (app *App) authMiddleware(authFunc func(http.ResponseWriter, *http.Request, *db.Utente)) http.HandlerFunc {
	return app.utenteMiddleware(func(w http.ResponseWriter, r *http.Request, utente *db.Utente) {
		if utente == nil {
			http.Redirect(w, r, "/login", http.StatusFound)
			return
		}

		authFunc(w, r, utente)
	})
}

// urlForLoginWithRedirect creates a url string of the form "/login?redirect={request_uri}" that is used by the login page to redirect to the previous page on successfull login.
func urlForLoginWithRedirect(r *http.Request) string {
	redirectParams := url.Values{
		"redirect": {r.URL.RequestURI()},
	}

	return "/login?" + redirectParams.Encode()
}

func (app *App) authRecallMiddleware(authFunc func(http.ResponseWriter, *http.Request, *db.Utente)) http.HandlerFunc {
	return app.utenteMiddleware(
		func(w http.ResponseWriter, r *http.Request, utente *db.Utente) {
			// Se l'utente non è loggato rimanda alla pagina di login con un parametro di redirect per poter tornare automaticamente alla pagina precedente dopo l'accesso.
			if utente == nil {
				http.Redirect(w, r, urlForLoginWithRedirect(r), http.StatusFound)
				return
			}

			authFunc(w, r, utente)
		},
	)
}

func (app *App) GetUtenteFromRequest(w http.ResponseWriter, r *http.Request) (*db.Utente, error) {
	session, err := app.CookieSessionStore.Get(r, "utente")
	if err != nil {
		return nil, err
	}

	utenteId, ok := session.Values["id"].(int)
	if !ok {
		return nil, nil
	}

	utente, err := app.Database.GetUtente(utenteId)
	if err != nil {
		return nil, err
	}

	log.Println("Successfull session authentication for:", utente)

	err = sessions.Save(r, w)
	if err != nil {
		return nil, err
	}

	return utente, nil
}

func (app *App) addRoutes() {

	// Main pages
	addHomepageRoutes(app)
	addListaLibriRoutes(app)
	addSingloLibroRoutes(app)

	// With authentication
	addPrendiInPrestitoRoutes(app)
	addPrestitoRoutes(app)
	addContribuisciRoutes(app)

	// User pages
	addLoginRoutes(app)
	addLogoutRoutes(app)
	addPersonalRoutes(app)
	addDeleteAccountRoutes(app)

	// Static files
	/*
		Visto che è tipo la millesima volta che mi dimentico come funge questa cosa,
		StripPrefix taglia il prefisso all'url della richiesta in arrivo quindi ad esempio:

		Url("/static/{filename}") -> "{filename}" -> File("./web/static/{filename}")
	*/
	app.Router.
		PathPrefix("/static/").
		Handler(http.StripPrefix("/static/", http.FileServer(http.Dir("./web/static"))))
}

type DevelopmentConfig struct {
	ExtraHtml template.HTML
}

func (devConfig *DevelopmentConfig) GetExtraHtml() template.HTML {
	if devConfig == nil {
		return ""
	} else {
		return devConfig.ExtraHtml
	}
}

type AppConfig struct {
	// Development is nil in production mode while in dev mode contains livereload info
	Development *DevelopmentConfig

	CookieSessionSecret string
}

func NewApp() *App {
	return NewAppWithConfig(&AppConfig{
		Development:         nil,
		CookieSessionSecret: "the secret",
	})
}

func NewAppWithConfig(config *AppConfig) *App {
	r := mux.NewRouter()

	db, err := db.NewDatabaseConnection()
	if err != nil {
		log.Fatal(err)
	}

	app := &App{
		config: config,

		Router:   r,
		Database: db,

		CookieSessionStore: sessions.NewCookieStore([]byte(config.CookieSessionSecret)),

		templateCache: map[string]*Page{},

		Server: &http.Server{
			Addr:         ":8080",
			Handler:      r,
			ReadTimeout:  5 * time.Second,
			WriteTimeout: 10 * time.Second,
			IdleTimeout:  120 * time.Second,
		},
	}

	app.addRoutes()

	return app
}
