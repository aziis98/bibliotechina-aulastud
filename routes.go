package bibliotechina

import (
	"fmt"
	"html/template"
	"net/http"
	"strconv"

	"github.com/aziis98/bibliotechina/db"
	"github.com/aziis98/bibliotechina/utils"
	"github.com/davecgh/go-spew/spew"
	"github.com/gorilla/mux"
)

var messaggioOperazioneNonSupportata = utils.Object{
	"Titolo": "Operazione non supportata",
	"Messaggio": template.HTML(`Questa operazione non è ancora supportata, in caso di estrema necessità puoi contattare direttamente l'admin 		aziis98 (Antonio De Lucreziis) <a href="mailto:antonio.delucreziis@gmail.com">via email</a> o cercando di persona al PHC o in Aula Studenti.`),
}

func addHomepageRoutes(app *App) {
	homepagePage := app.GetTemplate("./web/views/homepage.html")

	app.Router.
		HandleFunc("/",
			app.utenteMiddleware(func(w http.ResponseWriter, r *http.Request, utente *db.Utente) {
				app.RenderPage(w, homepagePage, utils.Object{
					"Utente": utente,
				})
			})).
		Methods("GET")
}

func addLoginRoutes(app *App) {
	loginPage := app.GetTemplate("./web/views/login.html")

	// GET /login
	//  Alternativamente anche "GET /login?redirect={url}" se si è arrivati alla pagina di login partendo da un'altra pagina
	app.Router.
		HandleFunc("/login",
			app.utenteMiddleware(func(w http.ResponseWriter, r *http.Request, utente *db.Utente) {
				// Se per qualche motivo l'utente è già loggato torna alla homepage
				if utente != nil {
					http.Redirect(w, r, "/", http.StatusFound)
					return
				}

				// Passa alla pagina di login tutto il path (eg. "/login?redirect={url}") per fare la richiesta POST corretta.
				app.RenderPage(w, loginPage, utils.Object{
					"RequestURI": r.URL.RequestURI(),
				})
			}),
		).
		Methods("GET")

	// POST /login
	//  Alternativamente anche "POST /login?redirect={url}" se si è arrivati alla pagina di login partendo da un'altra pagina
	app.Router.
		HandleFunc("/login", func(w http.ResponseWriter, r *http.Request) {
			err := app.Login(w, r, r.PostFormValue("email"), r.PostFormValue("password"))

			if err != nil {
				http.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}

			redirectPath := r.FormValue("redirect")

			if redirectPath == "" {
				redirectPath = "/"
			}

			http.Redirect(w, r, redirectPath, http.StatusFound)
		}).
		Methods("POST")

}

func addLogoutRoutes(app *App) {
	app.Router.
		HandleFunc("/logout",
			app.authMiddleware(func(w http.ResponseWriter, r *http.Request, utente *db.Utente) {
				err := app.Logout(w, r)
				if err != nil {
					http.Error(w, err.Error(), http.StatusInternalServerError)
					return
				}

				http.Redirect(w, r, "/", http.StatusFound)
			}),
		)
}

func addDeleteAccountRoutes(app *App) {
	app.Router.
		HandleFunc("/delete_account",
			app.authMiddleware(func(w http.ResponseWriter, r *http.Request, utente *db.Utente) {
				app.RenderMessaggio(w, r, messaggioOperazioneNonSupportata)
			}),
		)
}

func addPersonalRoutes(app *App) {
	utentePage := app.GetTemplate("./web/views/personale.html")

	// Pagina utente
	app.Router.
		HandleFunc("/me",
			app.authMiddleware(func(w http.ResponseWriter, r *http.Request, utente *db.Utente) {
				prestiti, err := app.Database.GetPrestitiUtente(utente.Id)
				if err != nil {
					http.Error(w, err.Error(), http.StatusInternalServerError)
					return
				}

				prestitiExt := []*db.PrestitoExt{}
				for _, p := range prestiti {
					pExt, err := app.Database.ExtendPrestito(p)
					if err != nil {
						http.Error(w, err.Error(), http.StatusInternalServerError)
						return
					}
					prestitiExt = append(prestitiExt, pExt)
				}

				if err != nil {
					http.Error(w, err.Error(), http.StatusInternalServerError)
					return
				}

				app.RenderPage(w, utentePage, utils.Object{
					"Utente":   utente,
					"Prestiti": prestitiExt,
				})
			}),
		).
		Methods("GET")
}

func addListaLibriRoutes(app *App) {

	libriPage := app.GetTemplate("./web/views/libri.html")

	// Elenco dei libri disponibili
	app.Router.
		HandleFunc("/libri",
			app.utenteMiddleware(func(w http.ResponseWriter, r *http.Request, utente *db.Utente) {
				libri, err := app.Database.GetLibriConAutori()
				if err != nil {
					http.Error(w, "Errore nell'ottenere la lista dei libri", http.StatusInternalServerError)
					return
				}

				app.RenderPage(w, libriPage, utils.Object{
					"Utente": utente,
					"Libri":  libri,
				})
			}),
		).
		Methods("GET")

}

func addSingloLibroRoutes(app *App) {
	libroPage := app.GetTemplate("./web/views/libro.html")

	// GET /libro/{book_id}
	app.Router.
		HandleFunc("/libro/{book_id}",
			app.utenteMiddleware(func(w http.ResponseWriter, r *http.Request, utente *db.Utente) {
				bookIdParam, ok := mux.Vars(r)["book_id"]
				if !ok {
					http.Error(w, `"book_id" must be present`, http.StatusInternalServerError)
					return
				}

				bookId, err := strconv.Atoi(bookIdParam)
				if err != nil {
					http.Error(w, err.Error(), http.StatusInternalServerError)
					return
				}

				libro, err := app.Database.GetLibroConAutori(bookId)
				if err != nil {
					http.Error(w, err.Error(), http.StatusInternalServerError)
					return
				}

				prestiti, err := app.Database.GetPrestitiLibro(bookId)
				if err != nil {
					http.Error(w, err.Error(), http.StatusInternalServerError)
					return
				}

				numCopieDisponibili, _, err := app.Database.GetNumeroLibriDisponibiliTotali(bookId)
				if err != nil {
					http.Error(w, err.Error(), http.StatusInternalServerError)
					return
				}

				app.RenderPage(w, libroPage, utils.Object{
					"Utente":                 utente,
					"Libro":                  libro,
					"Prestiti":               prestiti,
					"NumeroCopieDisponibili": numCopieDisponibili,
				})
			}),
		).
		Methods("GET").
		Name("libro")
}

func addPrendiInPrestitoRoutes(app *App) {
	prestitoPage := app.GetTemplate("./web/views/prendi-in-prestito.html")

	// GET /prendi-in-prestito?book_id={uid}
	// Invia la pagina per il prestito di un libro
	app.Router.HandleFunc("/prendi-in-prestito",
		app.authRecallMiddleware(
			func(w http.ResponseWriter, r *http.Request, utente *db.Utente) {
				optionalBookId := r.FormValue("book_id")

				bookId, err := strconv.Atoi(optionalBookId)
				if err != nil {
					http.Error(w, fmt.Sprintf(`"book_id" should be an integer, got instead %q`, optionalBookId), 500)
					return
				}

				libro, err := app.Database.GetLibroConAutori(bookId)
				if err != nil {
					http.Error(w, err.Error(), http.StatusInternalServerError)
					return
				}

				app.RenderPage(w, prestitoPage, utils.Object{
					"Utente": utente,
					"Libro":  libro,
				})
			},
		),
	).Methods("GET")

	// POST /prendi-in-prestito { book_id, ok?, cancel? }
	// Salva un prestito nel database
	app.Router.HandleFunc("/prendi-in-prestito",
		app.authRecallMiddleware(
			func(w http.ResponseWriter, r *http.Request, utente *db.Utente) {
				// Ottine l'id del libro da prendere in prestito
				oBookId := r.FormValue("book_id")
				bookId, err := strconv.Atoi(oBookId)
				if err != nil {
					http.Error(w, fmt.Sprintf(`"book_id" should be an integer, got instead %q`, oBookId), 500)
					return
				}

				// Check if the form was actually confirmed
				isOk := r.FormValue("ok") != ""

				if isOk {
					prestito, err := app.Database.CreatePrestito(bookId, utente.Id)
					if err != nil {
						http.Error(w, err.Error(), 500)
						return
					}

					app.RenderMessaggio(w, r, utils.Object{
						"Titolo": "Prestito eseguito con successo",
						"Messaggio": template.HTML(`
						<p>
							Poi trovare la lista dei libri che hai presto in prestito nella tua <a href="/me">pagina utente</a>.
						</p>
						<p>
							<code>` + spew.Sdump(prestito) + `</code>
						</p>
				`),
					})
				} else {
					http.Redirect(w, r, fmt.Sprintf("/libro/%d", bookId), http.StatusFound)
				}
			},
		),
	).Methods("POST")
}

func addPrestitoRoutes(app *App) {

	resoPage := app.GetTemplate("./web/views/prestito.html")

	// GET /prestito/{prestito_uid}
	// Reso di un libro preso in prestito
	app.Router.HandleFunc("/prestito/{uuid}",
		app.authMiddleware(func(w http.ResponseWriter, r *http.Request, utente *db.Utente) {
			uuid, ok := mux.Vars(r)["uuid"]
			if !ok {
				http.Error(w, "missing uuid from url", http.StatusInternalServerError)
				return
			}

			prestito, err := app.Database.GetPrestito(uuid)
			if err != nil {
				http.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}

			prestitoExt, err := app.Database.ExtendPrestito(prestito)
			if err != nil {
				http.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}

			app.RenderPage(w, resoPage, utils.Object{
				"Utente":   utente,
				"Prestito": prestitoExt,
			})
		}),
	).Methods("GET")

	// POST /restituisci-prestito { prestito_id }
	app.Router.HandleFunc("/restituisci-prestito", func(w http.ResponseWriter, r *http.Request) {
		oPrestitoId := r.FormValue("prestito_id")

		if len(oPrestitoId) == 0 {
			http.Error(w, "missing prestito_uuid from request", http.StatusInternalServerError)
			return
		}

		_, err := app.Database.TerminaPrestito(oPrestitoId)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		http.Redirect(w, r, "/prestito/"+oPrestitoId, http.StatusFound)
	}).Methods("POST")
}

func addContribuisciRoutes(app *App) {

	contribuisciPage := app.GetTemplate("./web/views/contribuisci.html")

	// GET /contribuisci
	// Contribuisci un libro alla bibliotechina
	app.Router.
		HandleFunc("/contribuisci", func(w http.ResponseWriter, r *http.Request) {
			app.RenderPage(w, contribuisciPage, utils.Object{})
		}).
		Methods("GET")

	// POST /contribuisci { libro_name, utente_id, ... }
	app.Router.
		HandleFunc("/contribuisci", func(w http.ResponseWriter, r *http.Request) {
			app.RenderMessaggio(w, r, messaggioOperazioneNonSupportata)
		}).
		Methods("POST")

}
