package main

import (
	"context"
	"log"
	"os"
	"os/signal"
	"time"

	"github.com/aziis98/bibliotechina"
	"github.com/fsnotify/fsnotify"
	"github.com/jaschaephraim/lrserver"

	flag "github.com/spf13/pflag"
)

var (
	programMode string
)

func init() {
	log.SetFlags(log.Ldate | log.Ltime | log.Lshortfile)

	flag.StringVar(&programMode, "mode", "production", `Mode of execution: production, development`)
	flag.Parse()

	log.Println("mode:", programMode)
}

func setupDevelopment(config *bibliotechina.AppConfig) {
	// Set string to append to all bodies
	config.Development = &bibliotechina.DevelopmentConfig{
		ExtraHtml: `<script src="http://localhost:35729/livereload.js"></script>`,
	}

	fileMapper := map[string]string{
		"views/prendi-in-prestito.html": "prendi-in-prestito",
		"views/prestito.html":           "prestito",
		"views/contribuisci.html":       "contribuisci",
		"views/homepage.html":           "homepage",
		"views/libro.html":              "libro",
		"views/messaggio.html":          "messaggio",
		"views/reso.html":               "reso",
		"views/libri.html":              "libri",
	}

	// Create watcher
	watcher, err := fsnotify.NewWatcher()
	if err != nil {
		log.Fatal(err)
	}

	lr := lrserver.New(lrserver.DefaultName, lrserver.DefaultPort)
	go lr.ListenAndServe()

	// Register directories
	err = watcher.Add("./web/views")
	if err != nil {
		log.Fatal(err)
	}
	err = watcher.Add("./web/static")
	if err != nil {
		log.Fatal(err)
	}

	go func() {
		lastEvent := time.Now()

		for {
			select {
			case event := <-watcher.Events:
				// Throttle reload calls
				if time.Since(lastEvent).Seconds() < 1 {
					continue
				}

				if event.Op&fsnotify.Write == fsnotify.Write {
					log.Println("modified file:", event.Name)
					actualUrl, ok := fileMapper[event.Name]
					if !ok {
						lr.Reload(event.Name)
					} else {
						lr.Reload(actualUrl)
					}
				}

				lastEvent = time.Now()
			case err := <-watcher.Errors:
				log.Println("error:", err)
			}
		}
	}()
}

func main() {
	config := &bibliotechina.AppConfig{}

	if programMode == "development" {
		setupDevelopment(config)
	}

	app := bibliotechina.NewAppWithConfig(config)

	// Start the server
	go func() {
		log.Printf("Running server on address %v", app.Server.Addr)
		app.Server.ListenAndServe()
	}()

	// Wait for an interrupt
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)
	<-c

	// Attempt a graceful shutdown
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	app.Server.Shutdown(ctx)
}
