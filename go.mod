module github.com/aziis98/bibliotechina

go 1.16

require (
	github.com/davecgh/go-spew v1.1.1
	github.com/fsnotify/fsnotify v1.4.9
	github.com/google/uuid v1.3.0
	github.com/gorilla/mux v1.8.0
	github.com/gorilla/sessions v1.2.1
	github.com/gorilla/websocket v1.4.2 // indirect
	github.com/jaschaephraim/lrserver v0.0.0-20171129202958-50d19f603f71
	github.com/mattn/go-sqlite3 v1.14.7
	github.com/smartystreets/goconvey v1.6.4 // indirect
	github.com/spf13/pflag v1.0.5
	golang.org/x/crypto v0.0.0-20210711020723-a769d52b0f97
	gopkg.in/fsnotify.v1 v1.4.7 // indirect
)
