package db

func (database *Database) GetUtente(id int) (*Utente, error) {
	var (
		email            string
		nomeCompleto     string
		passwordSaltHash string
	)

	row := database.conn.QueryRow(`
		SELECT email, nome_completo, password_salthash FROM utenti WHERE id == ?
	`, id)

	err := row.Scan(&email, &nomeCompleto, &passwordSaltHash)
	if err != nil {
		return nil, err
	}

	return &Utente{id, email, nomeCompleto, passwordSaltHash}, nil
}

func (database *Database) GetUtenteByEmail(email string) (*Utente, error) {
	var (
		id               int
		nomeCompleto     string
		passwordSaltHash string
	)

	row := database.conn.QueryRow(`
		SELECT id, nome_completo, password_salthash FROM utenti WHERE email == ?
	`, email)

	err := row.Scan(&id, &nomeCompleto, &passwordSaltHash)
	if err != nil {
		return nil, err
	}

	return &Utente{id, email, nomeCompleto, passwordSaltHash}, nil
}
