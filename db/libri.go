package db

func (database *Database) GetLibroConAutori(id int) (*LibroConAutori, error) {
	var (
		titolo      string
		numeroCopie int
		argomento   string
		note        string
	)

	row := database.conn.QueryRow(`
		SELECT titolo, numero_copie, argomento, note FROM libri l WHERE l.id == ?
	`, id)

	err := row.Scan(&titolo, &numeroCopie, &argomento, &note)
	if err != nil {
		return nil, err
	}

	autori := []*Autore{}

	rowsAutori, err := database.conn.Query(`
		SELECT al.autore_id, a.nome FROM autorelibro al, autori a WHERE al.libro_id == ? AND al.autore_id == a.id;
	`, id)
	if err != nil {
		return nil, err
	}

	for rowsAutori.Next() {
		var (
			autoreId   int
			autoreNome string
		)

		err := rowsAutori.Scan(&autoreId, &autoreNome)
		if err != nil {
			return nil, err
		}

		autori = append(autori, &Autore{
			autoreId,
			autoreNome,
		})
	}

	return &LibroConAutori{
		Libro{
			id,
			titolo,
			numeroCopie,
			argomento,
			note,
		},
		autori,
	}, nil
}

func (database *Database) GetLibriConAutori() ([]*LibroConAutori, error) {

	libri := []*LibroConAutori{}
	mapIdToLibro := map[int]*LibroConAutori{}

	// Get all books list
	rowsLibri, err := database.conn.Query(`
		SELECT id, titolo, numero_copie, argomento, note FROM libri ORDER BY titolo;
	`)
	if err != nil {
		return nil, err
	}
	defer rowsLibri.Close()

	for rowsLibri.Next() {
		var (
			id          int
			titolo      string
			numeroCopie int
			argomento   string
			note        string
		)

		err := rowsLibri.Scan(&id, &titolo, &numeroCopie, &argomento, &note)
		if err != nil {
			return nil, err
		}

		libro := &LibroConAutori{
			Libro{
				id,
				titolo,
				numeroCopie,
				argomento,
				note,
			},
			[]*Autore{},
		}

		mapIdToLibro[id] = libro
		libri = append(libri, libro)
	}

	rowsAutori, err := database.conn.Query(`
		SELECT al.libro_id, al.autore_id, a.nome FROM autorelibro al, autori a WHERE al.autore_id == a.id;
	`)
	if err != nil {
		return nil, err
	}
	defer rowsAutori.Close()

	for rowsAutori.Next() {
		var (
			libroId    int
			autoreId   int
			autoreNome string
		)

		err := rowsAutori.Scan(&libroId, &autoreId, &autoreNome)
		if err != nil {
			return nil, err
		}

		autore := &Autore{
			autoreId,
			autoreNome,
		}

		libro := mapIdToLibro[libroId]
		libro.Autori = append(libro.Autori, autore)
	}

	return libri, nil
}

func (database *Database) GetNumeroLibriPrestati(libroId int) (int, error) {
	var disponibili int

	// Conta i libri senza una data di reso che sono proprio quelli prestati
	row := database.conn.QueryRow(`
		SELECT count(*) FROM prestiti WHERE libro_id == ? AND data_reso IS NULL
	`, libroId)

	err := row.Scan(&disponibili)
	if err != nil {
		return 0, err
	}

	return disponibili, nil
}

func (database *Database) GetNumeroLibriTotali(libroId int) (int, error) {
	var totali int

	row := database.conn.QueryRow(`
		SELECT numero_copie FROM libri WHERE id == ?
	`, libroId)

	err := row.Scan(&totali)
	if err != nil {
		return 0, err
	}

	return totali, nil
}

func (database *Database) GetNumeroLibriDisponibiliTotali(libroId int) (disponibili int, totali int, err error) {
	var prestati int

	prestati, err = database.GetNumeroLibriPrestati(libroId)
	if err != nil {
		return
	}

	totali, err = database.GetNumeroLibriTotali(libroId)
	if err != nil {
		return
	}

	disponibili = totali - prestati
	return
}
