package db

import (
	"database/sql"

	_ "github.com/mattn/go-sqlite3" // Import go-sqlite3 library
)

// Database is just an alias for a sqlite database
type Database struct {
	conn *sql.DB
}

func NewDatabaseConnection() (*Database, error) {
	db, err := sql.Open("sqlite3", "./store.db")
	if err != nil {
		return nil, err
	}

	stmts := []string{
		`PRAGMA foreign_keys = ON`,
		`CREATE TABLE IF NOT EXISTS libri(
			id INTEGER PRIMARY KEY,
			
			titolo TEXT NOT NULL,
			numero_copie INTEGER NOT NULL,
			argomento TEXT NOT NULL,
			note TEXT NOT NULL,

			owner_id INTEGER NOT NULL,

			FOREIGN KEY (owner_id) REFERENCES utenti(id)
		)`,
		`CREATE TABLE IF NOT EXISTS autori(
			id INTEGER PRIMARY KEY,
			owner_id INTEGER NOT NULL,
			nome TEXT NOT NULL,

			FOREIGN KEY (owner_id) REFERENCES utenti(id)
		)`,
		`CREATE TABLE IF NOT EXISTS autorelibro(
			autore_id INTEGER NOT NULL,
			libro_id INTEGER NOT NULL,
			
			FOREIGN KEY (autore_id) REFERENCES libri(id), 
			FOREIGN KEY (libro_id) REFERENCES autori(id) 
		)`,
		`CREATE TABLE IF NOT EXISTS utenti(
			id INTEGER PRIMARY KEY,
			
			email TEXT NOT NULL UNIQUE,
			nome_completo TEXT,
			
			password_salthash TEXT NOT NULL
		)`,
		`CREATE TABLE IF NOT EXISTS prestiti(
			id TEXT NOT NULL PRIMARY KEY,
			
			data_prestito TEXT NOT NULL,
			data_reso TEXT,

			libro_id INTEGER NOT NULL,
			utente_id INTEGER NOT NULL,

			FOREIGN KEY(libro_id) REFERENCES libri(id),
			FOREIGN KEY(utente_id) REFERENCES utenti(id)
		)`,
	}

	for _, stmt := range stmts {
		db.Exec(stmt)
	}

	return &Database{db}, nil
}
