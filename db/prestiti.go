package db

import (
	"database/sql"
	"time"

	"github.com/google/uuid"

	"github.com/aziis98/bibliotechina/utils"
)

func (database *Database) CreatePrestito(libroId, utenteId int) (*Prestito, error) {
	var (
		id               string
		dataInizio       string
		nullableDataFine sql.NullString
	)

	uuid := uuid.Must(uuid.NewRandom())
	timestamp := time.Now().Format(time.RFC3339)

	row := database.conn.QueryRow(`
		INSERT INTO 
			prestiti (id, data_prestito, data_reso, libro_id, utente_id) 
		VALUES 	
			(?, ?, NULL, ?, ?)
		RETURNING 
			id, data_prestito, data_reso;
	`, uuid.String(), timestamp, libroId, utenteId)

	err := row.Scan(&id, &dataInizio, &nullableDataFine)
	if err != nil {
		return nil, err
	}

	return &Prestito{
		id,
		dataInizio,
		nullableDataFine.String,
		libroId,
		utenteId,
	}, nil
}

func (database *Database) TerminaPrestito(prestitoId string) (*Prestito, error) {
	var (
		dataInizio       string
		nullableDataFine sql.NullString
		libroId          int
		utenteId         int
	)

	timestamp := time.Now().Format(time.RFC3339)

	row := database.conn.QueryRow(`
		UPDATE 
			prestiti
		SET
			data_reso = ?
		WHERE
			id == ?
		RETURNING 
			data_prestito, data_reso, libro_id, utente_id;
	`, timestamp, prestitoId)

	err := row.Scan(&dataInizio, &nullableDataFine, &libroId, &utenteId)
	if err != nil {
		return nil, err
	}

	return &Prestito{
		prestitoId,
		dataInizio,
		nullableDataFine.String,
		libroId,
		utenteId,
	}, nil
}

func (database *Database) GetPrestito(uuid string) (*Prestito, error) {
	var (
		dataInizio  string
		optDataFine sql.NullString
		libroId     int
		utenteId    int
	)

	row := database.conn.QueryRow(`
		SELECT data_prestito, data_reso, libro_id, utente_id FROM prestiti WHERE id == ?
	`, uuid)

	err := row.Scan(&dataInizio, &optDataFine, &libroId, &utenteId)
	if err != nil {
		return nil, err
	}

	return &Prestito{uuid, dataInizio, optDataFine.String, libroId, utenteId}, nil
}

func (database *Database) GetPrestitiLibro(libroId int) ([]*Prestito, error) {

	prestiti := []*Prestito{}

	rowsPrestiti, err := database.conn.Query(`
		SELECT 
			id, data_prestito, data_reso, libro_id, utente_id 
		FROM 
			prestiti 
		WHERE 
			libro_id == ?
		ORDER BY
			data_prestito
		DESC
	`, libroId)
	if err != nil {
		return nil, err
	}

	for rowsPrestiti.Next() {
		var (
			id          string
			dataInizio  string
			optDataFine sql.NullString
			libroId     int
			utenteId    int
		)

		err := rowsPrestiti.Scan(&id, &dataInizio, &optDataFine, &libroId, &utenteId)
		if err != nil {
			return nil, err
		}

		prestiti = append(prestiti, &Prestito{
			id,
			dataInizio,
			optDataFine.String,
			libroId,
			utenteId,
		})
	}

	return prestiti, nil
}

func (database *Database) GetPrestitiUtente(utenteId int) ([]*Prestito, error) {

	prestiti := []*Prestito{}

	rowsPrestiti, err := database.conn.Query(`
		SELECT 
			id, data_prestito, data_reso, libro_id, utente_id 
		FROM 
			prestiti 
		WHERE 
			utente_id == ?
		ORDER BY
			data_prestito
		DESC
	`, utenteId)
	if err != nil {
		return nil, err
	}

	for rowsPrestiti.Next() {
		var (
			id          string
			dataInizio  string
			optDataFine sql.NullString
			libroId     int
			utenteId    int
		)

		err := rowsPrestiti.Scan(&id, &dataInizio, &optDataFine, &libroId, &utenteId)
		if err != nil {
			return nil, err
		}

		prestiti = append(prestiti, &Prestito{
			id,
			dataInizio,
			optDataFine.String,
			libroId,
			utenteId,
		})
	}

	return prestiti, nil
}

func (db *Database) ExtendPrestito(prestito *Prestito) (*PrestitoExt, error) {
	// Retrive libro con autori from db
	libroConAutori, err := db.GetLibroConAutori(prestito.LibroId)
	if err != nil {
		return nil, err
	}

	// Retrive full utente from db
	utente, err := db.GetUtente(prestito.UtenteId)
	if err != nil {
		return nil, err
	}

	return &PrestitoExt{
		prestito.Id,
		utils.ParseTime(prestito.DataInizio),
		utils.ParseTime(prestito.DataFine),
		libroConAutori,
		utente,
	}, nil
}
