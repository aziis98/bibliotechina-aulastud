package db

import "time"

// TODO: Questo file subirà un grnade refactor, non mi piace proprio questa cosa che al mometo ogni nuova variante deve prendere un nome lunghissimo.

type Autore struct {
	Id   int
	Nome string
}

type AutoreConProprietario struct {
	Autore
	OwnerId int
}

type Libro struct {
	Id          int
	Titolo      string
	NumeroCopie int
	Argomento   string
	Note        string
}

type LibroConProprietario struct {
	Libro
	OwnerId int
}

type LibroConAutori struct {
	Libro
	Autori []*Autore
}

// AutoriInlineString prende un libro con autori e concatena i vari autori in una stringa comoda da usare in HTML.
func (lca LibroConAutori) AutoriInlineString() string {
	s := ""

	for i, autore := range lca.Autori {
		if i == 0 {
			// First
			s += autore.Nome
		} else if i < len(lca.Autori)-1 {
			// Between
			s += ", " + autore.Nome
		} else {
			// Last
			s += " e " + autore.Nome
		}
	}

	return s
}

type Utente struct {
	Id int

	Email        string
	NomeCompleto string

	PasswordSaltHash string
}

// Prestito è la struct che rappresenta una row del database per un prestito
type Prestito struct {
	Id string

	DataInizio, DataFine string

	LibroId, UtenteId int
}

// PrestitoExt è la versione "semantica estesa" di un prestito semplice, i due tempi sono proprio *time.Time (il secondo può veramente essere nullable effettivamente) e "LibroId" e "UtenteId" sono diventati veramente dei campi "LibroConAutori" e "Utente".
//
// TODO: Struct di questo tipo sono veramente molto comode da usare per renderizzare i template però la nomenclatura ed i le funzioni di "estensione" per il momento lasciano molto a desiderare
type PrestitoExt struct {
	Id string

	// These two have to be nullable
	DataInizio *time.Time
	DataFine   *time.Time

	// More structured data
	LibroConAutori *LibroConAutori
	Utente         *Utente
}
