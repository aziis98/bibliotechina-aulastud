
function computeLetterIndex(query, text) {
    const letterIndex = new Map();

    new Set(query).forEach(c => letterIndex.set(c, []));

    [...text].forEach((c, j) => {
        if (letterIndex.has(c)) letterIndex.get(c).push(j);
    });

    return letterIndex;
}

// Lower is better, 0 is optimal for a perfect match at the start of the text.
function getFuzzyMatchScore(query, text) {
    const letterIndex = computeLetterIndex(query, text);

    let score = 0;

    for (let i = 1; i < query.length; i++) {
        const [c1, c2] = query.substr(i - 1, 2);

        const indices1 = letterIndex.get(c1);
        const indices2 = letterIndex.get(c2);
        const distances = [];

        for (const j1 of indices1) {
            for (const j2 of indices2) {
                if (j1 < j2) {
                    distances.push(j2 - j1 - 1);
                }
            }
        }

        const minSeqCharDist = Math.min(Infinity, ...distances);

        score += minSeqCharDist === Infinity ? 5 : minSeqCharDist;
    }

    const nearStartBonus = text.indexOf(query);
    return score + (nearStartBonus === -1 ? 1.0 : nearStartBonus / text.length);
}

function normalizeString(s) {
    return s.trim().replace(/\s+/g, ' ').toLowerCase();
}

const $searchInputField = document.querySelector('#search-book');

const $books = document.querySelector('.books');
const $bookArray = document.querySelectorAll('.books .book');

const books = [];

$bookArray.forEach($book => {
    const id = parseInt($book.dataset.libroId);
    const content = normalizeString($book.textContent);

    books.push({
        id,
        $el: $book,
        content,
    });
});

$searchInputField.addEventListener('input', e => {

    const query = normalizeString($searchInputField.value);
    const scoreMap = [];
    
    console.time('Search benchmark');
    books.forEach(book => {
        scoreMap[book.id] = getFuzzyMatchScore(query, book.content);
    });
    console.timeEnd('Search benchmark');

    // Sort books in ascending order
    books.sort((a, b) => scoreMap[a.id] - scoreMap[b.id]);

    books.forEach((book, i) => {
        book.$el.dataset.score = scoreMap[book.id];
        book.$el.style.order = i;
    });
});





