
# Bibliotechina

## Schema del Database

Questa è una descrizione della struttura del database

```sql
CREATE TABLE utenti(
	id INTEGER PRIMARY KEY,
	email TEXT NOT NULL UNIQUE,
	nome_completo TEXT,
	password_salthash TEXT NOT NULL
);
CREATE TABLE libri(
	id INTEGER PRIMARY KEY,
	titolo TEXT NOT NULL,
	numero_copie INTEGER NOT NULL,
	argomento TEXT NOT NULL,
	note TEXT NOT NULL,
	owner_id INTEGER NOT NULL,
	FOREIGN KEY (owner_id) REFERENCES utenti(id)
);
CREATE TABLE autori(
	id INTEGER PRIMARY KEY,
	owner_id INTEGER NOT NULL,
	nome TEXT NOT NULL,
	FOREIGN KEY (owner_id) REFERENCES utenti(id)
);
CREATE TABLE autorelibro(
	autore_id INTEGER NOT NULL,
	libro_id INTEGER NOT NULL,
	FOREIGN KEY (autore_id) REFERENCES libri(id),
	FOREIGN KEY (libro_id) REFERENCES autori(id)
);
CREATE TABLE IF NOT EXISTS "prestiti"(
    id TEXT NOT NULL PRIMARY KEY,
    data_prestito TEXT NOT NULL,
    data_reso TEXT,
    libro_id INTEGER NOT NULL,
    utente_id INTEGER NOT NULL,
    FOREIGN KEY(libro_id) REFERENCES libri(id),
    FOREIGN KEY(utente_id) REFERENCES utenti(id)
);
```

## Usage

```bash
go run cmd/server/main.go
```

## Development

```bash
go run cmd/server/main.go --mode development
```

or you have `entr` installed then just run

```bash
find . -name '*.go' | entr -r go run cmd/server/main.go --mode development
```