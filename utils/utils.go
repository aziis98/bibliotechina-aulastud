package utils

import (
	"time"
)

// Object creator util
type Object map[string]interface{}

func (target Object) Apply(sources ...Object) Object {
	for _, source := range sources {
		for k, v := range source {
			target[k] = v
		}
	}

	return target
}

func ParseTime(value string) *time.Time {
	t, err := time.Parse(time.RFC3339, value)
	if err != nil {
		return nil
	} else {
		return &t
	}
}

func FormatTime(t time.Time) string {
	return t.Format(time.RFC3339)
}
