# Documentazione

Pagina di documentazione per il progetto della Bibliotechina.

### `/data` 

Cartella che raccogli vari dati usati per la creazione del database iniziale di tutti i libri.

#### `/data/libri.tsv`

File originale TSV di tutti i libri.

#### ...

## Note

### Dilemma View/Model

Come fare meglio la seguente cosa?

```go

type Prestito struct {
	Id string

	DataInizio, DataFine string

	LibroId, UtenteId int
}

type PrestitoExt struct {
	Id string

	DataInizio *time.Time
	DataFine   *time.Time

	LibroConAutori *LibroConAutori
	Utente         *Utente
}

```

### Import sqlite DB

```bash
rm store.db
sqlite3 store.db
> .read data/backup_2.sql
```

### Extract authors from libri

```sql
WITH RECURSIVE split(id, author, str) AS (
    SELECT rowid as id, '', authors || ',' FROM libri_tmp
    UNION ALL SELECT
    id,
    substr(str, 0, instr(str, ',')),
    substr(str, instr(str, ',')+1)
    FROM split WHERE str!=''
) 
SELECT id, author
FROM split
WHERE author <> '';
```

Inline version:

```sql
WITH RECURSIVE split(id, author, str) AS (SELECT rowid as id, '', authors || ',' FROM (SELECT rowid as id, "Autore" as authors from libri_tmp LIMIT 10) UNION ALL SELECT id, substr(str, 0, instr(str, ',')), substr(str, instr(str, ',')+1) FROM split WHERE str!='') SELECT id, author FROM split WHERE author!='';
```

### Tutti gli autori di un certo libro

```sql
SELECT al.libro_id, a.id, a.nome FROM autorelibro al, autori a WHERE al.autore_id == a.id AND al.libro_id == 2;
```

### Tutti i libri di un certo autore

```sql
SELECT l.id as libro_id, l.titolo, a.id as autore_id, a.nome FROM autorelibro al, libri l, autori a WHERE al.libro_id == l.id AND a.id == al.autore_id AND al.autore_id == 45;
```
