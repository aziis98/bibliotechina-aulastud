
CREATE TABLE IF NOT EXISTS prestiti_new(
    id TEXT NOT NULL PRIMARY KEY,
    
    data_prestito TEXT NOT NULL,
    data_reso TEXT,
    
    libro_id INTEGER NOT NULL,
    utente_id INTEGER NOT NULL,

    FOREIGN KEY(libro_id) REFERENCES libri(id),
    FOREIGN KEY(utente_id) REFERENCES utenti(id)
);

INSERT INTO 
    prestiti_new 
SELECT 
    printf('%08X', abs(random())) as id,
    data_prestito,
    data_reso,
    libro_id,
    utente_id
FROM 
    prestiti;

DROP TABLE prestiti;

ALTER TABLE prestiti_new RENAME TO prestiti;

